

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxMenuItem;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.String;
import java.lang.System;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

public class NotepadMenu extends Frame implements ActionListener,TextListener,WindowListener
{
	TextArea ta=new TextArea("");
	boolean flag,flag1,flag2,flag3,flag4;
	String str="",str3="",cutcopystr="";
	MenuItem new1,open,save,saveAs,pgSetup,print,exit,undo,cut,copy,paste,delete,find,findNext,replace,goTo,selectAll,timeDate,font,vhelp,abt;
	CheckboxMenuItem wordWrap,sbar;
	NotepadMenu()
	{
		super("Untitled - Notepad");
		setSize(1000,700);
		setVisible(true);
		add(ta);
		MenuBar mbar=new MenuBar();
		Menu file=new Menu("File");
		Menu edit=new Menu("Edit");
		Menu format=new Menu("Format");
		Menu view=new Menu("View");
		Menu help=new Menu("Help");
		new1=new MenuItem("New",new MenuShortcut(KeyEvent.VK_N));
		open=new MenuItem("Open...",new MenuShortcut(KeyEvent.VK_O));
		save=new MenuItem("Save",new MenuShortcut(KeyEvent.VK_S));
		saveAs=new MenuItem("Save As...");
		pgSetup=new MenuItem("Page Setup...");
		print=new MenuItem("Print...",new MenuShortcut(KeyEvent.VK_P));
		exit=new MenuItem("Exit");
		undo=new MenuItem("Undo",new MenuShortcut(KeyEvent.VK_Z));
		cut=new MenuItem("Cut",new MenuShortcut(KeyEvent.VK_X));
		copy=new MenuItem("Copy",new MenuShortcut(KeyEvent.VK_C));
		paste=new MenuItem("Paste",new MenuShortcut(KeyEvent.VK_V));
		delete=new MenuItem("Delete");
		find=new MenuItem("Find...",new MenuShortcut(KeyEvent.VK_F));
		findNext=new MenuItem("Find Next");
		replace=new MenuItem("Replace...",new MenuShortcut(KeyEvent.VK_H));
		goTo=new MenuItem("Go To...",new MenuShortcut(KeyEvent.VK_G));
		selectAll=new MenuItem("Select All",new MenuShortcut(KeyEvent.VK_A));
		timeDate=new MenuItem("Time/Date");
		wordWrap=new CheckboxMenuItem("Word Wrap");
		font=new MenuItem("Font...");
		sbar=new CheckboxMenuItem("Status Bar");
		vhelp=new MenuItem("View Help");
		abt=new MenuItem("About Notepad");
		file.add(new1);
		file.add(open);
		file.add(save);
		file.add(saveAs);
		file.addSeparator();
		file.add(pgSetup);
		file.add(print);
		file.addSeparator();
		file.add(exit);
		edit.add(undo);
		edit.addSeparator();
		cut.setEnabled(false);
		edit.add(cut);
		copy.setEnabled(false);
		edit.add(copy);
		paste.setEnabled(false);
		edit.add(paste);
		delete.setEnabled(false);
		edit.add(delete);
		edit.addSeparator();
		find.setEnabled(false);
		edit.add(find);
		findNext.setEnabled(false);
		edit.add(findNext);
		edit.add(replace);
		edit.add(goTo);
		edit.addSeparator();
		edit.add(selectAll);
		edit.add(timeDate);
		format.add(wordWrap);
		format.add(font);
		view.add(sbar);
		help.add(vhelp);
		help.addSeparator();
		help.add(abt);
		mbar.add(file);
		mbar.add(edit);
		mbar.add(format);
		mbar.add(view);
		mbar.add(help);
		setMenuBar(mbar);
		ta.addTextListener(this);
		new1.addActionListener(this);
		save.addActionListener(this);
		saveAs.addActionListener(this);
		open.addActionListener(this);
		exit.addActionListener(this);
		cut.addActionListener(this);
		copy.addActionListener(this);
		paste.addActionListener(this);
		delete.addActionListener(this);
		selectAll.addActionListener(this);
		find.addActionListener(this);
		addWindowListener(this);
		ta.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent me)
			{
				enabled();
			}
		});
		ta.addMouseMotionListener(new MouseMotionAdapter(){
			public void mouseDragged(MouseEvent me)
			{
				enabled();
			}
		});
		ta.addKeyListener( new KeyAdapter(){
			public void keyTyped(KeyEvent ke)
			{
				//if(flag)
					flag3=true;
			}
			public void keyReleased(KeyEvent ke)
			{
				enabled();
			}
		}
		);
			
	}
	public void enabled()
	{
					if(!ta.getSelectedText().equals(""))
					{
						cut.setEnabled(true);
						copy.setEnabled(true);
						delete.setEnabled(true);
					}
					else
					{
							cut.setEnabled(false);
							copy.setEnabled(false);
							delete.setEnabled(false);
					}
	}
	public void exit()
	{
			NewDialog nd;
			if(!ta.getText().equals(""))
			{
				if(!flag || flag && flag3)
				{
					 nd=new NewDialog(this,this);
					if(nd.flag1)
							System.exit(0);
				}
				else
					System.exit(0);

			}
			else
				System.exit(0);
	}
	public void windowClosing(WindowEvent we)
	{
		exit();
	}
	public void windowClosed(WindowEvent we){};
	public void windowOpened(WindowEvent we){};
	public void windowIconified(WindowEvent we){};
	public void windowDeiconified(WindowEvent we){};
	public void windowActivated(WindowEvent we){};
	public void windowDeactivated(WindowEvent we){};
	public void textValueChanged(TextEvent te)
	{
		if(!ta.getText().equals(""))
		{
			find.setEnabled(true);
			findNext.setEnabled(true);
		}
		else
		{
			find.setEnabled(false);
			findNext.setEnabled(false);
		}
	}
	public void saveAs()
	{
		
			FileDialog fd=new FileDialog(this,"Save As",FileDialog.SAVE);
			fd.setVisible(true);
			if(fd.getDirectory()!=null && fd.getFile()!=null)
			{
				if(fd.getFile().indexOf(".")==-1 || fd.getFile().indexOf(".dt")!=-1)
				{
					if(fd.getFile().indexOf(".dt")==-1)
									fd.setFile(fd.getFile()+".dt");
					File f=new File(fd.getDirectory()+fd.getFile());
					String msg="";
					int i=0;
					try
					{
						PrintWriter writer=new PrintWriter(new FileOutputStream(f),true);
						int j=0;
						String str1="";
						while(j<ta.getText().length())
						{
							int a=((int)ta.getText().charAt(j))+4;
							if(a>=256)
								a%=256;
							char c=(char)a;
							str1+=c;
							if(j<ta.getText().length())
								j++;
						}
						writer.println(str1);
						writer.close();
						while(fd.getFile().charAt(i)!='.')
						{
							msg+=fd.getFile().charAt(i);
							if(i<fd.getFile().length())
									i++;
						}					
						setTitle(msg+" - Notepad");
						str=fd.getDirectory()+fd.getFile();
					}
						catch (IOException e){}
				}
				else
				{
					File f=new File(fd.getDirectory()+fd.getFile());
					try
					{
						String msg="";
						int i=0;
						PrintWriter writer=new PrintWriter(new FileOutputStream(f),true);
						writer.println(ta.getText());
						writer.close();
						while(fd.getFile().charAt(i)!='.')
						{
							msg+=fd.getFile().charAt(i);
							if(i<fd.getFile().length())
									i++;
						}					
						setTitle(msg+" - Notepad");
						str=fd.getDirectory()+fd.getFile();

					}
					catch (IOException e){}
				}
						flag=true;
						flag2=false;
						flag3=false;

			}
	}
	public void save()
	{
				File f;
				if(flag && !flag2)
					 f=new File(str);
				else
				{
					 f=new File(str3);
					 str=str3;//DOUBT
				}
					try
					{
						PrintWriter writer=new PrintWriter(new FileOutputStream(f),true);
					if(str.indexOf(".dt")!=-1 || str3.indexOf(".dt")!=-1){
						int j=0;
						String str1="";
						while(j<ta.getText().length())
						{
							int a=((int)ta.getText().charAt(j))+4;
							if(a>=256)
								a%=256;
							char c=(char)a;
							str1+=c;
							if(j<ta.getText().length())
								j++;
						}
						writer.println(str1);
						writer.close();
					}
					else{

						writer.println(ta.getText());
						writer.close();	
					}
					flag=true;
					flag2=false;
					flag3=false;

				}
				catch (IOException e){}
	}
	public void open()
	{
			
			FileDialog fd=new FileDialog(this,"Open",FileDialog.LOAD);
			fd.setVisible(true);
			if(fd.getDirectory()!=null && fd.getFile()!=null)
			{
				File f=new File(fd.getDirectory()+fd.getFile());
				try
				{
					BufferedReader in=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
					String line,msg="",str="",str1="";
					int i=0,j=0;
					while((line=in.readLine())!=null)
					{
						str+=line;
						str+="\n";
					}
					str=str.substring(0,str.length()-1);					
					if(fd.getFile().indexOf(".dt")!=-1)
					{
						while(j<str.length())
						{
							int a=((int)str.charAt(j))-4;
							if(a<0)
								a+=256;
							char c=(char)a;
							str1+=c;
							if(j<str.length())
								j++;
						}
						ta.setText(str1);
					}
					else
					{
						ta.setText(str);
					}
					flag3=false;
					in.close();
					while(fd.getFile().charAt(i)!='.')
					{
						msg+=fd.getFile().charAt(i);
						if(i<fd.getFile().length())
							i++;
					}					
					str3=fd.getDirectory()+fd.getFile();
					setTitle(msg+" - Notepad");
					flag2=true;
					flag=true;
				}
				catch (IOException e){}
			}
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==new1)
		{
			if(!flag && !flag2)
			{
				NewDialog nd;
				if(!ta.getText().equals(""))
				{
						 nd=new NewDialog(this,this);
					if(nd.flag1)
							ta.setText("");
				}
			}
			else
			{													
					NewDialog nd;
						if(flag3)
						{
							 nd=new NewDialog(this,this);
							if(nd.flag1)
							{
									ta.setText("");
									setTitle("Untitled - Notepad");
									flag=false;
							}
						}
						else
						{
							setTitle("Untitled - Notepad");
							ta.setText("");
							flag=false;
						}
					}
		}
		else if(ae.getSource()==saveAs)
					saveAs();
		else if(ae.getSource()==open)
		{
			FileDialog fd=new FileDialog(this,"Open",FileDialog.LOAD);
			NewDialog nd;
			if(!flag)
			{
				if(!ta.getText().equals(""))
				{
						 nd=new NewDialog(this,this);
						 if(!nd.flag && !nd.flag2 && nd.flag1)
								open();
				}
				else
					open();
					
			}
			else if(flag && flag3)
			{
					nd=new NewDialog(this,this);
					 if(!nd.flag && !nd.flag2 && nd.flag1)
								open();
			}
					
			else
				open();

		}
		else if(ae.getSource()==save)
		{
				if(!flag)
						saveAs();
				else
					save();
		}
		else if(ae.getSource()==exit)
					exit();
		else if(ae.getSource()==cut)
		{
			paste.setEnabled(true);
			cutcopystr=ta.getSelectedText();
			int a=ta.getText().indexOf(ta.getSelectedText(),ta.getSelectionStart());
			ta.replaceRange("",a,a+ta.getSelectedText().length());
			cut.setEnabled(false);
			copy.setEnabled(false);
			delete.setEnabled(false);
		}
		else if(ae.getSource()==copy)
		{
					paste.setEnabled(true);
					cutcopystr=ta.getSelectedText();
		}
		else if(ae.getSource()==paste)
		{
				if(ta.getSelectedText().equals(""))
								ta.insert(cutcopystr,ta.getCaretPosition());
				else
				{
						int a=ta.getText().indexOf(ta.getSelectedText(),ta.getSelectionStart());
						ta.replaceRange("",a,a+ta.getSelectedText().length());
						ta.insert(cutcopystr,ta.getSelectionStart());
				}
		}
		else if(ae.getSource()==delete)
		{
					int a=ta.getText().indexOf(ta.getSelectedText(),ta.getSelectionStart());
					ta.replaceRange("",a,a+ta.getSelectedText().length());
					cut.setEnabled(false);
					copy.setEnabled(false);
					delete.setEnabled(false);
		}
		else if(ae.getSource()==selectAll)
			ta.select(0,ta.getText().length());
		else if(ae.getSource()==find)
		{
			new FindDialog(this,this);
		}
	}
	public static void main(String args[])
	{
		new NotepadMenu();
	}
}
class FindDialog extends Dialog implements ActionListener
{
	Label fw;
	TextField tf;
	Button fn,cancel;
	Checkbox c;
	Panel p,g,p1,p2,p3;
	NotepadMenu nm;
	FindDialog(Frame f,NotepadMenu nm)
	{
		super(f,"Find");
		setSize(500,200);
		setVisible(true);
		this.nm=nm;
		p=new Panel();
		p1=new Panel();
		p2=new Panel();
		p3=new Panel();
		g=new Panel();
		fw=new Label("Find what:",Label.LEFT);
		tf=new TextField(30);
		p.setLayout(new FlowLayout(FlowLayout.LEFT));
		p.add(fw);
		p.add(tf);
		g.setLayout(new GridLayout(2,1,0,10));
		fn=new Button("Find Next");
		cancel=new Button("Cancel");
		fn.setEnabled(false);
		g.add(fn);
		g.add(cancel);
		p1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		p1.add(g);
		c=new Checkbox("Match Case");
		p2.setLayout(new FlowLayout(FlowLayout.LEFT));
		p2.add(c);
		JLabel label = new JLabel("Direction");
		JRadioButton up = new JRadioButton("Up");
		JRadioButton down = new JRadioButton("Down");
		p2.setLayout(new FlowLayout());
		p2.add(label);
		p2.add(up);
		p2.add(down);
		add(p,BorderLayout.WEST);
		add(p1,BorderLayout.EAST);
		add(p2,BorderLayout.SOUTH);
		fn.addActionListener(this);
		tf.addTextListener(new TextListener(){
			public void textValueChanged(TextEvent te){
				if(!tf.getText().equals(""))
						fn.setEnabled(true);
				else
					fn.setEnabled(false);
			}
		});
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				dispose();
			}
		});
	}
	/*int count()
	{
		int count=0;
		if(!flag)
		{
			for(int i=0;i<=index1;i++)
			{
				if(tf.getText().charAt(i)=='\n')
					count++;
			}
		}
		else
		{
			for(int i=0;i<=index;i++)
			{
				if(tf.getText().charAt(i)=='\n')
					count++;
			}
		}
		return count;
	}
	void replace()
	{
			tf.setText(tf.getText().replaceFirst(nu.tf1.getText(),nu.tf2.getText()));
		
	}
	void replaceAll()
	{
			tf.setText(tf.getText().replaceAll(nu.tf1.getText(),nu.tf2.getText()));
		
	}
	void find()
	{
		index=nm.ta.getText().indexOf(tf.getText());
		if(index==-1)
		{
		}
		else
		{
			flag=true;
			a=index;
			c=count();
			a-=c;
			nu.l2.setText("");
			System.out.println(index);
			nu.index=index;
			nu.index1=index;
			tf.select(a,a+nu.tf1.getText().length());
			tf.requestFocus();
			System.out.println(tf.getSelectedText());
		}
	}
	void findNext()
	{
		if(nu.flag1 || index1==-1)
		{
			index1=nu.index;
			nu.flag1=false;
		}
		index1=tf.getText().indexOf(nu.tf1.getText(),index1+1);
		System.out.println(index1);
		if(index1==-1)
		{
			nu.index1=-1;
			tf.setText(tf.getText());
			nu.l2.setText("No occurrences Found!");
			
		}
		else
		{
				flag=false;
				nu.index1=0;
				a=index1;
				c=count();
				a-=c;
				tf.select(a,a+nu.tf1.getText().length());
				tf.requestFocus();
				System.out.println(tf.getSelectedText());
			
		}
	}*/
	public Insets getInsets()
	{
		return new Insets(50,20,20,20);
	}
	public void actionPerformed(ActionEvent ae){
			if(ae.getSource()==fn){

			}
	}
}
class NewDialog extends Dialog implements ActionListener
{
	NotepadMenu nm;
	Label l;
	Button save,dsave,cancel;
	boolean flag,flag1,flag2;
	NewDialog(Frame f,NotepadMenu nm)
	{
		super(f,"Notepad",true);
		setSize(500,200);
		this.nm=nm;
		setBackground(Color.WHITE);
		if(!nm.flag)
				 l=new Label("Do you want to save changes to Untitled?",Label.CENTER);
		else if(nm.flag2 && nm.flag)
		{
			 l=new Label("Do you want to save changes to"+nm.str3+"?",Label.CENTER);
		}
		else
			l=new Label("Do you want to save changes to "+nm.str+"?",Label.CENTER);

		l.setForeground(Color.BLUE);
		Panel p=new Panel();
		add(l);
		save=new Button("Save");
		dsave=new Button("Don't Save");
		cancel=new Button("Cancel");
		p.setLayout(new FlowLayout(FlowLayout.LEFT));
		p.add(save);
		p.setLayout(new FlowLayout(FlowLayout.CENTER));
		p.add(dsave);
		p.setLayout(new FlowLayout(FlowLayout.RIGHT));
		p.add(cancel);
		p.setLayout(new FlowLayout(FlowLayout.CENTER));
		add(p,BorderLayout.SOUTH);
		cancel.addActionListener(this);
		save.addActionListener(this);
		dsave.addActionListener(this);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we)
			{
				dispose();
				
			}
		});
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae)
	{
		
		if(ae.getSource()==cancel)
		{
						flag=true;
						dispose();
		}
		else if(ae.getSource()==dsave)
		{
				flag1=true;
				dispose();
				//nm.ta.setText("");
		}
		else
		{
			flag2=true;
			dispose();
			if(!nm.flag)
				nm.saveAs();
			else
			{
				nm.save();
			}
		}
	}
}